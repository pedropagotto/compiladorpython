#importação da lib ply renomenado a mesma para ply
import ply.lex as ply;

reserved = (
    'pscanf', 'printf', 'pif', 'pfor', 'pwhile',
);
tokens = reserved + (
    'IGUAL', 'SOMA', 'SUB', 'MULT', 'DIV', 'EXP',
    'PAREN', 'MEN', 'MENI', 'MAI', 'MAII', 'DIF',
    'pInt', 'pFloat', 'pString',
    'ID', 'NEWLINE', 'TIPO', 'CONDICIONAL','REPETICAO', 'IMPRESSAO', 'LEITURA'
);

#definindo comentarios e linhas a serem ignoradas
t_ignore = ' \t'
t_ignore_COMMENTLINE='\#.*'

def t_PREM(t):
    r'REM .*'
    return t;

#definindo ID de palavras reservadas
def t_ID(t):
    r'P[a-zA-Z][a-zA-Z0-9]*'
    if t.value in reserved:
        t.type = t.value;
    return t;


#Gramatica do compilador
t_TIPO = r'pInt|pFloat|pDouble|pChar';
t_CONDICIONAL = r'pif';
t_REPETICAO = r'pfor|pwhile';
t_IMPRESSAO = r'printf';
t_LEITURA = r'pscanf';
t_IGUAL = r'=';
t_SOMA = r'\+';
t_SUB = r'-';
t_MULT = r'\*';
t_EXP = r'\^';
t_DIV = r'/';
t_PAREN = r'\p';
t_MEN = r'<';
t_MENI = r'<=';
t_MAI = r'>';
t_MAII = r'>=';
t_DIF = r'<>';
t_pInt = r'\d+';
t_pFloat = r'((\d*\.\d+)(E[\+-]?\d+)?|([1-9]\d*E[\+-]?\d+))';
t_pString = r'\".*?\"';


#definindo nova linha
def t_NEWLINE(t):
    r'\n'
    t.lexer.lineno += 1;
    return t;

#Tratando erros
def t_error(t):
    print("Informação inválida %s" % t.value[0]);
    t.lexer.skip(1);

# Criando o analisador lexico
lexer = ply.lex();

#String com o programa na linguagem "p"
programap = '''
pInt Paux
Paux = 30
if Paux < 30 p
pwhile Pi < 0 p
pif px < 10 p
Pa = Pf + Pa - Pg
Pprintf p "Este código funciona" p''';

#chamando a classe com input do nosso programa para que ele consiga realizar a interpretação com as regras
lexer.input(programap);


#laço de repetição que realiza a leitura dos tokens que foram interpretados.
while True:
    pytokens = lexer.token();
    if not pytokens:
        break
    print(pytokens);